import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToDevicePidotax = functions.firestore
  .document("travels/{travelsId}")
  .onUpdate(async (change, context) => {

    var firstname: string = "";
    var lastname: string = "";
    var phone: string = "";
    var id: string = "";
    var suggestion : string = "";
    var tokens : string = "";



    const travelsId = context.params.travelsId;
    id = travelsId;
    const order = change.after.data();
    //const previsorder = change.before.data();
    const addressp = order.address_p;
    const addressd = order.address_d
    const distance = "" + order.distance;
    const time = order.time;
    const driverId = order.driverId
    const usersId = order.userId;
    suggestion = order.serviceCondition;
    console.log(usersId, suggestion);
    const latituded = "" + order.arrivalLatitude;
    const longituded = "" + order.arrivalLongitude;
    const latitude = "" + order.departureLatitude;
    const longitude = "" + order.departureLongitude;
    const positionId = order.positionId;
    const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";

    if (order.driverId != null && order.positionId == null && order.status == "pending") {
    
       db.collection('posicion').doc(positionId).update({"state" :false});
    
      interface users {
        phoneNumber: string
        firstName: string
        lastName: string
        imagen: string
        imagenPortada: string
        id: string
        birthDate: string
        email: string
        invitationCode: string
      }


      let userRef = await db.collection('users')
        .doc(usersId).collection('informacion')
      let alluser = await userRef.get();
      for (const doc of alluser.docs) {
        let obj: users = JSON.parse(JSON.stringify(doc.data()));

        firstname = obj.firstName
        lastname = obj.lastName
        phone = obj.phoneNumber

      }

      console.log(phone, time);




      interface users {
        token: string
      }


      const querySnapshot = await db
      .collection('drivers')
      .doc(driverId)
      .get();



    tokens = querySnapshot.get('token');
    console.log(tokens);



      const payload: admin.messaging.MessagingPayload = {
        notification: {
          title: 'Solicitud de Pidotax!',
          body: `del cliente:`,
          icon: imagenUrl,
          click_action: 'FLUTTER_NOTIFICATION_CLICK'
        },
        data: {
          tipo:"Solicitud",
          travelid: id,
          firstname: firstname,
          lastname: lastname,
          phoneNumber: phone,
          addressp: addressp,
          addressd: addressd,
          traveltime: time,
          distance: distance,
          latitude: latitude,
          longitude: longitude,
          latituded: latituded,
          longituded: longituded,
          suggestion: ""+order.serviceCondition


        }
      };

      return fcm.sendToDevice(tokens, payload).then(Response => {
        console.log('La notificacion se ha enviado correctamente')
      });
    }
  });

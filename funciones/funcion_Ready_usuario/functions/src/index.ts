import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToTaxiReadyUsuarioPidotax = functions.firestore
    .document("travels/{travelsId}")
    .onUpdate(async (change, context) => {

        const nuevoData = change.after.data();
        const travelId = context.params.travelsId;
        const route = nuevoData.route;
        const status = nuevoData.status;
        const driverId = nuevoData.driverId;
        const positionId = nuevoData.positionId;
        const birthDate = nuevoData.date;


        const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";

        var tokens = "";
        var tipo = "";
        var title = "";
        var descripcion = "";
        var lastname = "";
        var name = "";
        var phone = "";
        var placa = "";
        var url = "";
        var taxiId = "";


        if (status == "active" && positionId != null && route == "ready") {
       
                const querySnapshot = await db
                    .collection('drivers')
                    .doc(nuevoData.driverId)
                    .get();

                name = querySnapshot.get('firstName');
                lastname = querySnapshot.get('lastName');
                url = querySnapshot.get('url');
                phone = querySnapshot.get('phoneNumber');
                taxiId = querySnapshot.get('taxi');
               

                interface users {
                    token: string
                }

                let userRef =  db.collection('users')
                    .doc(nuevoData.userId).collection('token')
                let alluser = await userRef.get();
                for (const doc of alluser.docs) {
                    let obj: users = JSON.parse(JSON.stringify(doc.data()));
                    tokens = obj.token
                }

                const querySnapshot2 = await db
                    .collection('taxis')
                    .doc(taxiId)
                    .get();

                placa = querySnapshot2.get('placa');

                tipo = "ready";
                title = "'El Taxi ha llegado!'";
                descripcion = "20 metros";
                console.log("usuario1:" + tokens);

            const payload: admin.messaging.MessagingPayload = {
                notification: {
                    title: title,
                    body: descripcion,
                    icon: imagenUrl,
                    click_action: 'FLUTTER_NOTIFICATION_CLICK'
                },
                data: {
                    tipo: tipo != undefined ? tipo : "",
                    driverId: driverId != undefined ? driverId : "",
                    birthDate : birthDate != undefined ? birthDate : "",
                    firstname: name != undefined ? name : "" ,
                    lastname: lastname != undefined ? lastname : "",
                    taxi: placa != undefined ? placa : "",
                    phone: phone != undefined ? phone : "",
                    url: url != undefined ? url : "",
                    travelId: travelId != undefined ? travelId : "",
             
                }
            };

            return fcm.sendToDevice(tokens, payload).then(Response => {
                console.log(Response);
                console.log('La notificacion se ha enviado correctamente')
            });


        }
    });
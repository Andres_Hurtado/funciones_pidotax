import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToTaxiReadyConductorPidotax = functions.firestore
    .document("travels/{travelsId}")
    .onUpdate(async (change, context) => {

        const nuevoData = change.after.data();
        const travelId = context.params.travelsId;
        const route = nuevoData.route;
        const status = nuevoData.status;
        const positionId = nuevoData.positionId;

       // const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";

        var tokens = "";
        var tipo = "";


        if (status == "active" && positionId != null && route == "ready") {
       
                const querySnapshot = await db
                    .collection('drivers')
                    .doc(nuevoData.driverId)
                    .get();

                tokens = querySnapshot.get('token');
     

                tipo = "ready";
                console.log("conductor token:" + tokens);

            const payload: admin.messaging.MessagingPayload = {
                data: {
                    tipo: tipo != undefined ? tipo : "",
                    travelId: travelId != undefined ? travelId : "",
                }
            };

            return fcm.sendToDevice(tokens, payload).then(Response => {
                console.log(Response);
                console.log('La notificacion se ha enviado correctamente')
            });


        }
    });
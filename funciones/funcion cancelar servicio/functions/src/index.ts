import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToDeviceCalcelConductorPidotax = functions.firestore
  .document("travels/{travelsId}")
  .onUpdate(async (change, context) => {

    const travelsId = context.params.travelsId;
    const nuevoData = change.after.data();
    const antiguaData = change.before.data();
    const driverId = nuevoData.driverId;
   // const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";
    var tokens = "";
    var cause = "";

      if (nuevoData.status == "Cancel 1" && antiguaData.status == "active" ) {
        console.log("Cancelo servicio Cliente");

        const querySnapshot = await db
          .collection('drivers')
          .doc(driverId)
          .get();



        tokens = querySnapshot.get('token');
        console.log(tokens);


        interface cause {
          cause: string
        }



        let userRef =  db.collection('travels')
          .doc(travelsId).collection('cause')
        let alluser = await userRef.get();
        for (const doc of alluser.docs) {
          let obj: cause = JSON.parse(JSON.stringify(doc.data()));
          console.log(obj.cause)
          cause = obj.cause;

        }
      
      const payload: admin.messaging.MessagingPayload = {
        data: {
          tipo: "Calcelado",
          porque: cause,
          servicio: "SERVICIO CANCELADO",
        }
      };

      return fcm.sendToDevice(tokens, payload).then(Response => {
        console.log(Response);
        console.log('La notificacion se ha enviado correctamente')
      });
  }
  });
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToDeviceRequestUsuarioPidotax = functions.firestore
  .document("travels/{travelsId}")
  .onUpdate(async (change, context) => {

    const travelsId = context.params.travelsId;
    const newValue = change.after.data();

    // ...or the previous value before this update
    //const previousValue = change.before.data();
    const newposictionId = newValue.positionId;
    //  const previousVposictionId = previousValue.positionId;

    const driverId = newValue.driverId;
    const userId = newValue.userId;


    var name: string = "";
    var taxiId: string = "";
    var phone: string = "";
    var token: string = "";
    var url: string = "";
    var lastname: string = "";
    var placa: string = "";

    const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";

    if (newposictionId != "" && newposictionId != null && newValue.status == "active" && newValue.route == null) {
 
        const querySnapshotDriver = await db
        .collection('drivers')
        .doc(driverId)
        .get();


        name = querySnapshotDriver.get('firstName');
        lastname = querySnapshotDriver.get('lastName');
        url = querySnapshotDriver.get('url');
        phone = querySnapshotDriver.get('phoneNumber');
        taxiId = querySnapshotDriver.get('taxi');  
        url = querySnapshotDriver.get('url');


      const querySnapshot = await db
        .collection('taxis')
        .doc(taxiId)
        .get();
      placa = querySnapshot.get('placa');

      interface users {
        token: string
      }


      let userRef = db.collection('users')
        .doc(userId).collection('token')
      let alluser = await userRef.get();
      for (const doc of alluser.docs) {
        let obj: users = JSON.parse(JSON.stringify(doc.data()));
        token = obj.token
      }


      const tokens = token;
      console.log(tokens);


      const payload: admin.messaging.MessagingPayload = {
        notification: {
          title: 'Solicitud de Taxi!',
          body: `asignado conductor`,
          icon: imagenUrl,
          click_action: 'FLUTTER_NOTIFICATION_CLICK'
        },
        data: {
          tipo: "Solicitud",
          id: travelsId,
          driverId: driverId,
          firstname: name,
          lastname: lastname,
          taxi: placa,
          phone: phone,
          url: url
        }
      };


      return fcm.sendToDevice(tokens, payload).then(Response => {
        console.log(Response);
        console.log('this is the notification')
      });
    }
  });

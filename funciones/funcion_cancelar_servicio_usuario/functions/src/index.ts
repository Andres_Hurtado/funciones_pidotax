import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToDeviceCalcelUsuarioPidotax = functions.firestore
  .document("travels/{travelsId}")
  .onUpdate(async (change, context) => {

    const nuevoData = change.after.data();
    const antiguaData = change.before.data();
    const userId = nuevoData.userId;
    const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";
    var tokens = "";
    var canceladoPor = "";
    var cause = "";

    if (nuevoData.status == "Cancel 2" && antiguaData.status == "active") {

        console.log("Cancelo servicio Taxi");

        interface users {
          token: string
        }


        let userRef =  db.collection('users')
          .doc(userId).collection('token')
        let alluser = await userRef.get();
        for (const doc of alluser.docs) {
          let obj: users = JSON.parse(JSON.stringify(doc.data()));

          tokens = obj.token


        }

        canceladoPor = "Taxi";
      
      const payload: admin.messaging.MessagingPayload = {
        notification: {
          title: 'El servicio fue cancelado!',
          body: `por el ${canceladoPor} :`,
          icon: imagenUrl,
          click_action: 'FLUTTER_NOTIFICATION_CLICK'
        },
        data: {
          tipo: "Calcelado",
          porque: cause,
          servicio: "SERVICIO CANCELADO",
        }
      };

      return fcm.sendToDevice(tokens, payload).then(Response => {
        console.log(Response);
        console.log('La notificacion se ha enviado correctamente')
      });
    }
  });
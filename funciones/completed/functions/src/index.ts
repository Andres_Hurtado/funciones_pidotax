import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

const db = admin.firestore();
const fcm = admin.messaging();

exports.sendToTaxiCompletedPidotax = functions.firestore
    .document("travels/{travelsId}")
    .onUpdate(async (change, context) => {

        const nuevoData = change.after.data();
        const travelId = context.params.travelsId;
        const route = nuevoData.route;
        const status = nuevoData.status;
        const driverId = nuevoData.driverId;
        const positionId = nuevoData.positionId;
        const birthDate = nuevoData.date;
        let listtokens: string[];


        const imagenUrl = "https://firebasestorage.googleapis.com/v0/b/pidotax-9ab80.appspot.com/o/Logo_Pidotax%2Fpidotax_logo.png?alt=media&token=02387f9c-fb4b-40b1-a2f3-bd51ac23d60e";

        var tokens1 = "";
        var tipo = "";
        var title = "";
        var descripcion = "";
        var lastname = "";
        var name = "";
        var phone = "";
        var placa = "";
        var url = "";
        var taxiId = "";
        var depertureLng = "";
        var depertureLat  = "";
        var arrivalLat = "";
        var arrivalLng = "";

        if (status == "completed" && positionId != null && route == "completed") {

                interface users {
                    token: string
                }

                let userRef =  db.collection('users')
                    .doc(nuevoData.userId).collection('token')
                let alluser = await userRef.get();
                for (const doc of alluser.docs) {
                    let obj: users = JSON.parse(JSON.stringify(doc.data()));

                    tokens1 = obj.token


                }

                const querySnapshot = await db
                    .collection('drivers')
                    .doc(nuevoData.driverId)
                    .get();


                name = querySnapshot.get('firstName');
                lastname = querySnapshot.get('lastName');
                url = querySnapshot.get('url');
                phone = querySnapshot.get('phoneNumber');
                taxiId = querySnapshot.get('taxi');
                depertureLat =( nuevoData.departureLatitude).toString();
                depertureLng = (nuevoData.departureLongitude).toString();
                arrivalLat = (nuevoData.arrivalLatitude).toString();
                arrivalLng = (nuevoData.arrivalLongitude).toString();
                tipo = "completed";
                title = "'El taxi completo el servicio!'";
                descripcion = "lugar destino";

                const querySnapshot2 = await db
                    .collection('taxis')
                    .doc(taxiId)
                    .get();

                placa = querySnapshot2.get('placa');

                console.log("usuario1:" + tokens1);
                listtokens = [tokens1];

                console.log("latitudes", depertureLat, depertureLng, arrivalLat, arrivalLng)

            const payload: admin.messaging.MessagingPayload = {
                notification: {
                    title: title,
                    body: descripcion,
                    icon: imagenUrl,
                    click_action: 'FLUTTER_NOTIFICATION_CLICK'
                },
                data: {
                    tipo: tipo  != undefined ? tipo : "",
                    driverId: driverId  != undefined ? driverId : "",
                    birthDate : birthDate != undefined ? birthDate : "",
                    firstname: name != undefined ? name : "",
                    lastname: lastname != undefined ? lastname : "",
                    taxi: placa != undefined ? placa : "",
                    phone: phone != undefined ? phone : "",
                    url: url != undefined ? url : "",
                    travelId: travelId != undefined ? travelId : "",
                    depertureL: depertureLat != undefined ? depertureLat : "",
                    depertureLg: depertureLng != undefined ? depertureLng : "",
                    arrivalL: arrivalLat != undefined ? arrivalLat : "",
                    arrivalLg: arrivalLng != undefined ? arrivalLng : "",
                }
            };

            return fcm.sendToDevice(listtokens, payload).then(Response => {
                console.log('La notificacion se ha enviado correctamente')
            });


        }
    });